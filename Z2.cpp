
#include "stdafx.h"
#include "iostream"
#include "fstream"
#include "cstdlib"
#include "string"
#include "conio.h"
using namespace std;

int licz_bin(string nazwa)
{
	ifstream sizeCheck(nazwa, ios::binary | ios::ate);
	int size = sizeCheck.tellg();
	//fstream::pos_type size = sizeCheck.tellg();
	size = size / sizeof(char);
	sizeCheck.close();
	return size;
}

int licz(string nazwa)
{
	ifstream Plik(nazwa);
	string liczba;
	int ilosc=0;

	while (getline(Plik, liczba))
		++ilosc;

	return ilosc;
}

void tek_zap(int *tab, int rozmiar)
{
	int i;
		fstream NowyPlik;
		NowyPlik.open("tekst.txt",ofstream::out);
		if (NowyPlik.is_open())
		{
			for (i = 0; i < rozmiar; i++)
			{
				NowyPlik << tab[i] << endl;
			}
		}
		
		NowyPlik.close();
		cout << "Zapisano do pliku tekstowego." << endl;
}

void tek_wcz(int *tab, int rozmiar)
{
	
	int i;
	rozmiar = licz("tekst.txt");
	//int *tab = new int[rozmiar];
	ifstream NowyPlik;
	NowyPlik.open("tekst.txt", ios::in);
	if (NowyPlik.is_open())
	{
		cout << "Oto wczytana tablica:" << endl;
		for (i = 0; i < rozmiar; i++)
		{
			NowyPlik >>tab[i];
			cout << tab[i] << endl;
		}
	}
	NowyPlik.close();
	cout << "Wczytano." << endl;
	//Tab = tab;
	
}

void bin_zap(int *tab, int rozmiar)
{
	ofstream NowyPlik("binar", ofstream::binary); 
	//cin >> tab->number1;
	char* bufor = new char[rozmiar];
	//NowyPlik.close();
	if (NowyPlik.is_open())
	{
		for (int i = 0; i < rozmiar; i++)
		{
			bufor[i] = (char)tab[i];
		}
		NowyPlik.write(bufor, rozmiar);
		NowyPlik.close();
	}
	delete[] bufor;
}

void bin_wcz(int *tab, int rozmiar)
{
	rozmiar = licz_bin("binar");
	ifstream NowyPlik("binar", ifstream::binary);
	//cin >> tab->number1;
	//char* bufor = new char[rozmiar];
	//NowyPlik.close();
	if (NowyPlik.is_open())
	{
		char* bufor = new char[rozmiar];
		NowyPlik.read(bufor, rozmiar);
		cout << "Oto wczytana tablica:" << endl;
		for (int i = 0; i < rozmiar; ++i)
		{
			tab[i] = (int)bufor[i];
			//Tab[i] = tab[i];
			cout << tab[i] << endl;
		}
		
		NowyPlik.close();
	}
	//delete[] bufor;
}

int main()
{
	int rozmiar;
	int max, i;
	int wybor;
	cout << "Podaj rozmiar dlugosc tablicy:";
	cin >> rozmiar;
	cout << endl << "Podaj max liczbe:";
	cin >> max;
	cout << endl;

	int *tab_p = new int[rozmiar];
	int *tab = new int[rozmiar];
	for (i = 0; i < rozmiar; i++)
	{
		tab[i] = rand() % max;
	}
	tab_p = tab;

	do {
		cout << "Witaj. Wybierz rozsadnie:" << endl;
		cout << "1. Zapis do pliku tekstowego." << endl;
		cout << "2. Zapis do pliku binarnego." << endl;
		cout << "3.Wczytaj tab z pliku binarnego." << endl;
		cout << "4.Wczytaj tab z pliku tekstowego." << endl;
		cout << "5.Wyswietl wwczytana tablice." << endl;
		cout << "6. Koniec." << endl;
		cin >> wybor;
		switch (wybor) {
		case 1:
			tek_zap(tab, rozmiar);
			break;

		case 2:
			bin_zap(tab, rozmiar);
			break;

		case 3:
			bin_wcz(tab_p, rozmiar);
			tab = tab_p;
			rozmiar = licz_bin("binar");
			break;

		case 4:
			tek_wcz(tab_p, rozmiar);
			tab = tab_p;
			rozmiar = licz("tekst.txt");
			break;

		case 5:
			{
			cout << "Oto tab:" << endl;
				for (int j = 0; j < rozmiar; j++)
				{
					cout << tab[j] << endl;
				}
			}	
		break;	

		default:
			"Brak takiego wyboru.";
			break;
		}
	} while (wybor != 6);

	return 0;
}
