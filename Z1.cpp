#include "stdafx.h"
#include "iostream"
#include "cstdlib"
using namespace std;

void wyp(int **Tab, int n, int m)
{
	int x = 0;
	int i, j;
	cout << "Podaj wartosc max:";
	cin >> x;
	cout << endl;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			Tab[i][j] = rand() % (x + 1);
		}
	}
}

void wysw(int **Tab, int n, int m)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			cout << Tab[i][j] << " ";
		}
		cout << endl;
	}
}

void max(int **Tab, int n, int m)
{
	int i, j;
	int MAX = 0;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			if (MAX < Tab[i][j])
			{
				MAX = Tab[i][j];
			}
		}
	}
	cout << "Max to:" << MAX << endl;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			if (MAX > Tab[i][j])
			{
				MAX = Tab[i][j];
			}
		}
	}
	cout << "Minimum to:" << MAX << endl;
}

int main()
{
	int err=0;
	int n = 0;
	int m = 0;
	int i = 0;
	int j = 0;
	int MAX=-1;
	//int Tab[10][10];
	cout << "Podaj liczbe wierszy:";
	std::cin >> n;
	cout << endl << "Podaj liczbe kolumn:";
	std::cin >> m;
	cout << endl;
	int **Tab = new int*[n];
	for (int p = 0; p < n; p++)
	{
		Tab[p] = new int[m];
	}
	
	int liczba = 0;
	do {
		cout << "Witaj w programie TABLICE, wybierz jedna z operacji:" << endl;
		cout << "1.Zmien rozmiar tablicy." << endl;
		cout << "2.Wybierz max wartosc tablicy i wypelnij ja." << endl;
		cout << "3.Wyswietl tablice." << endl;
		cout << "4.Znajdz najwieksza wartosc tablicy." << endl;
		cout << "5.Zakoncz." << endl;
		cin >> liczba;

		switch (liczba)
		{
		case 1:
		{
			cout << "Podaj liczbe wierszy:";
			std::cin >> n;
			cout << endl << "Podaj liczbe kolumn:";
			std::cin >> m;
			cout << endl;
			int **Tab = new int*[n];
			for (int p = 0; p < n; p++)
			{
				Tab[p] = new int[m];
			}
			err = 0;
		}
			break;

		case 2:
		{
			wyp(Tab, n, m);
			err++;
		}
			break;

		case 3:
			if (err != 0) {
				wysw(Tab, n, m);
			}
			else {
				cout << "Nie wypelniles tablicy." << endl;
			}
			break;

		case 4:
			if (err != 0) {
				max(Tab, n, m);
			}
			else {
				cout << "Nie wypelniles tablicy." << endl;
			}
			break;

		case 5:
			cout << "Zegnaj!" << endl;
				break;

		default: cout << liczba << ", ta opcja nie jest dostepna. \n";

			cout << endl;
		}
	} while (liczba != 5);

	return 0;
}
