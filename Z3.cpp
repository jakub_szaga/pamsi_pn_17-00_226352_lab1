#include "stdafx.h"
#include "iostream"
using namespace std;

int Potega(int x, int p)
{
	if (p == 0)
		return 1;

	return x*Potega(x, p - 1);
}

int Silnia(int x)
{
	if (x < 2)
		return 1;

	return x*Silnia(x - 1);
}

int main()
{
	int l, p;
	int wybor;
	do {
		cout << "Witaj, wybierz jedna z opcji:" << endl;
		cout << "1.Potegowanie." << endl;
		cout << "2.Silnia." << endl;
		cout << "3.Koniec" << endl;
		cin >> wybor;

		switch (wybor) {
		case 1:
		{
			cout << "Wybierz liczbe:" << endl;
			cin >> l;
			cout << "Wybierz jej potege:" << endl;
			cin >> p;
			cout << Potega(l, p) << endl;
		}
		break;

		case 2:
		{
			cout << "Wybierz liczbe:" << endl;
			cin >> l;
			cout << Silnia(l) << endl;
		}
		break;

		default:
			cout << "Brak takiej opcji" << endl;
			break;

		}
	} while (wybor != 3);

	return 0;
}
